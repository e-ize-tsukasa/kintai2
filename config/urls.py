"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView, RedirectView
from django.contrib.auth import views as auth_views
from kintai.views import top
from django.conf import settings
from django.conf.urls.static import static
import debug_toolbar  #本番時消す


# adminサイトの表示カスタマイズ
admin.site.site_title = 'e-ize管理画面'
admin.site.site_header = 'e-ize管理画面'
admin.site.index_title = 'メニュー'


urlpatterns = [
    path('admin/', admin.site.urls, name='admin'),
    path('', top.SampleTemplateView.as_view(), name='top'),
    path('kintai/master/', include('kintai.urls.master_urls')),
    path('kintai/roster/', include('kintai.urls.roster_urls')),
    path('kintai/salaried/', include('kintai.urls.salaried_urls')),
    path('kintai/management/', include('kintai.urls.management_urls')),
    path('login', RedirectView.as_view(url='/accounts/login/'), name='login'),
    path('accounts/', include('accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')),

    path('__debug__/', include(debug_toolbar.urls)),
]
# 印影画像格納先を指定
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)