import os

# 本番環境の設定を記述

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '64$i7%fwfl*+9rg1ozg%1$$++d85(h3ej48v#32hy@@uzd_tt@'

DEBUG = False

# 環境移行時に許可するホスト後で修正
ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrap4',
    'kintai',
    'axes',
    'accounts',
    'django_cleanup',  # ImageFieldのレコード削除時に画像も同時に削除するため
]

AUTHENTICATION_BACKENDS = [
    'axes.backends.AxesBackend',
    'django.contrib.auth.backends.ModelBackend',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'middleware.login_required_middleware.LoginRequiredMiddleware',
    'axes.middleware.AxesMiddleware',
    'global_login_required.GlobalLoginRequiredMiddleware',
]
# GlobalLoginRequiredMiddlewareを適用しないパス
PUBLIC_PATHS = [
	r'^/accounts/.*',
]


ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
)

WSGI_APPLICATION = 'config.wsgi.application'

# とりあえずこれで。本番のdb作ったら修正
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'officesys',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '3306',
        'ATOMIC_REQUESTS': True,
        'OPTIONS': {
            'charset': 'utf8mb4',
            'sql_mode': 'TRADITIONAL,NO_AUTO_VALUE_ON_ZERO',
        },
    }
}

# 本番のログ設定 ディレクトリ決まってから修正
# ローテーション設定も後々行う
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'formatters': {
#         'production': {
#             'format': '%(asctime)s [%(levelname)s] %(process)d %(thread)d '
#                       '%(pathname)s:%(lineno)d %(message)s'
#         },
#     },
#     'handlers': {
#         'file': {
#             'class': 'logging.FileHandler',
#             'filename': 'logs/django.log',  #環境に合わせて変更
#             'formatter': 'production',
#             'level': 'INFO',
#         },
#     },
#     'loggers': {
#         # 自作したログ出力
#         'kintai': {
#             'handlers': ['file'],
#             'level': 'INFO',
#             'propagate': False,
#         },
#         # Djangoの警告・エラー
#         'django': {
#             'handlers': ['file'],
#             'level': 'INFO',
#             'propagate': False,
#         },
#     },
# }

# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
   # {
   #    'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
   # },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
   # {
   #     'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
   # },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# 管理画面日本語化
LANGUAGE_CODE = 'ja'
TIME_ZONE = 'Asia/Tokyo'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# キャッシュの設定(axes使用のため)
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'axes_cache': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

AXES_CACHE = 'axes'
AXES_FAILURE_LIMIT = 5 #ログイン失敗5回まで
AXES_LOCKOUT_TEMPLATE = 'account_locked.html'



STATIC_URL = '/static/'
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

# メディアのパス。印影の画像を格納する想定。modelのImageFieldカラムに対応
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'


# ログインのモデルやリダイレクト先を設定
AUTH_USER_MODEL = 'kintai.User'
LOGIN_REDIRECT_URL = 'top'
LOGOUT_REDIRECT_URL = 'login'
LOGIN_URL = 'login'


LOGIN_EXEMPT_URLS=(
    r'^about\.html$',
)