from django.urls import path
from kintai.views.salaried import salaried_top, request, approval_status



urlpatterns = [
    path('', salaried_top.SampleTemplateView.as_view(), name='salaried_top'),
    path('request', request.SampleTemplateView.as_view(), name='salaried_request'),
    path('approval_status', approval_status.SampleTemplateView.as_view(), name='salaried_approval_status'),
]
