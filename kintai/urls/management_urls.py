from django.urls import path
from kintai.views.management import management_top, holiday, print_seal


urlpatterns = [
    path('', management_top.SampleTemplateView.as_view(), name='management_top'),
    path('holiday', holiday.SampleTemplateView.as_view(), name='management_holiday'),
    path('print_seal', print_seal.SampleTemplateView.as_view(), name='management_print_seal'),
]
