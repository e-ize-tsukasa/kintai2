from django.urls import path
from kintai.views.master import master_top, roster_approval, salaried_approval



urlpatterns = [
    path('', master_top.SampleTemplateView.as_view(), name='master_top'),
    path('roster_approval', roster_approval.SampleTemplateView.as_view(), name='master_roster_approval'),
    path('salaried_approval', salaried_approval.SampleTemplateView.as_view(), name='master_salaried_approval'),
]
