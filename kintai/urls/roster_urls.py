from django.urls import path
from kintai.views.roster import roster_top, edit, reference, approval_status



urlpatterns = [
    path('', roster_top.SampleFormView.as_view(), name='roster_top'),
    path('edit', edit.SampleTemplateView.as_view(), name='roster_edit'),
    path('reference', reference.SampleTemplateView.as_view(), name='roster_reference'),
    path('approval_status', approval_status.SampleTemplateView.as_view(), name='roster_approval_status'),
]
