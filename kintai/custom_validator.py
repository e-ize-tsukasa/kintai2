from django.core.validators import RegexValidator


en_half_regex = RegexValidator(regex=r'^[a-z]+$', message="半角英字のみ入力可能です")
num_half_regex = RegexValidator(regex=r'^[0-9]+$', message="半角数字のみ入力可能です")
en_and_num_half_regex = RegexValidator(regex=r'^[0-9a-z]+$', message="半角英数字のみ入力可能です")

jp_full_regex = RegexValidator(regex=u'^[ぁ-んァ-ヶー一-龠]+$', message='全角のひらがな・カタカナ・漢字で入力してください')
