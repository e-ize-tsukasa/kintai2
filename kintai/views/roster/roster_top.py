from django.shortcuts import render
from django.views.generic import TemplateView, FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from django import forms


import logging
logging.getLogger(__name__)


class SimpleForm(forms.Form):
    year_mm = forms.DateField(label='', widget=forms.SelectDateWidget())


class SampleFormView(FormView):
    form_class = SimpleForm

    template_name = "roster/roster_top.html"

    def form_valid(self, form):
        return super().form_valid(form)
