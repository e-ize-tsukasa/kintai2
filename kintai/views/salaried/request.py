from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


import logging
logging.getLogger(__name__)

class SampleTemplateView(TemplateView):
    template_name = "salaried/request.html"