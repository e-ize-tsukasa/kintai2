from django.shortcuts import render
from django.views.generic import TemplateView
from libpython.custom_mixin import RequiresSuperUserMixin


import logging
logging.getLogger(__name__)

class SampleTemplateView(RequiresSuperUserMixin, TemplateView):
    template_name = "master/roster_approval.html"