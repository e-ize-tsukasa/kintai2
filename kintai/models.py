from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MaxLengthValidator, MinLengthValidator, MinValueValidator, RegexValidator
from kintai.custom_validator import jp_full_regex, num_half_regex, en_half_regex

import os

class UserManager(UserManager):
    def _create_user(self, email, password, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)

class User(AbstractBaseUser, PermissionsMixin):
    user_id = models.AutoField(verbose_name='ユーザーid', primary_key=True, help_text="自動採番")
    username = models.CharField(_('username'), max_length=50, validators=[en_half_regex])
    email = models.EmailField(_('email address'), unique=True, max_length=50)
    last_name = models.CharField(_('last name'), max_length=50, validators=[jp_full_regex])
    first_name = models.CharField(_('first name'), max_length=50, validators=[jp_full_regex])
    enter_date = models.DateField(verbose_name='入社年月日')
    google_client_id = models.CharField(verbose_name='Googleクライアントid', max_length=255, blank=True, null=True, help_text="承認者は設定してください")
    google_client_sercret = models.CharField(verbose_name='Googleクライアントシークレット', max_length=50, blank=True, null=True, help_text="承認者は設定してください")
    is_superuser = models.BooleanField(verbose_name='承認者権限', default=False, help_text="勤務表・有給の承認が可能。管理サイトですべての編集が可能。")
    is_active = models.BooleanField(_('active'), default=True, help_text="論理削除する場合チェックを外す")
    is_staff = models.BooleanField(_('staff status'), default=True, help_text="djangoの仕様的にこのカラム無くすと修正するとこ多いから一応残す。")
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'last_name', 'first_name', 'enter_date']

    class Meta:
        db_table = 'm_user'
        verbose_name = verbose_name_plural = "ユーザー"
        indexes = [
            models.Index(fields=['email'], name='index_m_user_on_email'),
        ]

    def get_full_name(self):
        """ユーザー名(姓)+ユーザー名(名)"""
        full_name = '%s %s' % (self.last_name, self.first_name)
        return full_name.strip()

    def get_short_name(self):
        """ユーザー名(名)"""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """登録してるメールに送る"""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def __str__(self):
        return self.get_full_name()

class MPrintSeal(models.Model):

    def image_file_name(instance):
        return os.path.join('print_seal', str(instance.user.user_id))

    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    file_path = models.CharField(verbose_name='格納先パス', max_length=255)
    file_name = models.ImageField(verbose_name='ファイル名', upload_to=image_file_name)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    created_id = models.IntegerField(verbose_name='作成者id', blank=True, null=True, validators=[MaxLengthValidator(11)])
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    updated_id = models.IntegerField(verbose_name='更新者id', blank=True, null=True, validators=[MaxLengthValidator(11)])
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 'm_print_seal'
        verbose_name = verbose_name_plural = "印影マスタ"

    def __str__(self):
        return str(self.user)


class MClient(models.Model):
    client_id = models.AutoField(verbose_name='クライアントid', primary_key=True, help_text="自動採番")
    client_name = models.CharField(verbose_name='クライアント名', max_length=50)
    def_work_from = models.TimeField(verbose_name='標準勤務開始時間', max_length=4)
    def_work_to = models.TimeField(verbose_name='標準勤務終了時間', max_length=4)
    def_rest_from = models.TimeField(verbose_name='標準休憩開始時間', max_length=4)
    def_rest_to = models.TimeField(verbose_name='標準休憩終了時間', max_length=4)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 'm_client'
        verbose_name = verbose_name_plural = "常駐先マスタ"

    def __str__(self):
        return self.client_name


class TOfficeMonth(models.Model):
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    office_yy_mm = models.CharField(verbose_name='勤務年月', max_length=6)
    approval_status_id = models.ForeignKey('TApprovalStatus', verbose_name='承認状況ID', to_field="approval_status_id", on_delete=models.CASCADE)
    client_id = models.IntegerField(verbose_name='クライアントid', validators=[MaxLengthValidator(11)])
    start_salaried_day = models.DecimalField(verbose_name='月初有給残日数', max_digits=3, decimal_places=1)
    end_salaried_day = models.DecimalField(verbose_name='月末有給残日数', max_digits=3, decimal_places=1, blank=True, null=True)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    created_id = models.IntegerField(verbose_name='作成者id', validators=[MaxLengthValidator(11)])
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    updated_id = models.IntegerField(verbose_name='更新者id', validators=[MaxLengthValidator(11)])
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_office_month'
        verbose_name = verbose_name_plural = "勤務月"
        unique_together = (('user', 'office_yy_mm'),)

    def __str__(self):
        return self.office_yy_mm


class TOfficeMonthHistory(models.Model):
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    office_yy_mm = models.CharField(verbose_name='勤務年月', max_length=6)
    history_id = models.IntegerField(verbose_name='履歴id', validators=[MaxLengthValidator(11)], help_text="勤務月テーブルのユニーク毎にインクリメント")
    approval_status_id = models.ForeignKey('TApprovalStatus', verbose_name='承認状況ID', to_field="approval_status_id", on_delete=models.CASCADE)
    client_id = models.IntegerField(verbose_name='クライアントid', validators=[MaxLengthValidator(11)])
    start_salaried_day = models.DecimalField(verbose_name='月初有給残日数', max_digits=3, decimal_places=1)
    end_salaried_day = models.DecimalField(verbose_name='月末有給残日数', max_digits=3, decimal_places=1, blank=True, null=True)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    created_id = models.IntegerField(verbose_name='作成者id', validators=[MaxLengthValidator(11)])
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    updated_id = models.IntegerField(verbose_name='更新者id', validators=[MaxLengthValidator(11)])
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_office_month_history'
        verbose_name = verbose_name_plural = "勤務月履歴"
        unique_together = (('user', 'office_yy_mm', 'history_id'),)

    def __str__(self):
        return self.office_yy_mm


class TOfficeTime(models.Model):
    HOLIDAY_CHOICES = (
        ('0', '平日'),
        ('1', '土曜'),
        ('2', '日曜、祝日、祝祭日'),
    )
    OFFICE_FLG_CHOICES = (
        ('0', '通常勤務'),
        ('1', '休日'),
        ('2', '有給'),
        ('3', '欠勤'),
        ('4', '特別休日'),
        ('5', '振休'),
    )
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    office_yy_mm = models.CharField(verbose_name='勤務年月', max_length=6)
    office_date = models.IntegerField(verbose_name='日付', validators=[MaxLengthValidator(2)])
    office_day = models.CharField(verbose_name='曜日', max_length=1)
    holiday_flg = models.CharField(verbose_name='祝日フラグ', max_length=1, choices=HOLIDAY_CHOICES)
    office_from = models.CharField(verbose_name='常駐先出勤時間', max_length=5, blank=True, null=True)
    office_to = models.CharField(verbose_name='常駐先退勤時間', max_length=5, blank=True, null=True)
    office_rest = models.CharField(verbose_name='常駐先休憩時間', max_length=5, blank=True, null=True)
    office_work = models.CharField(verbose_name='常駐先稼働時間', max_length=5, blank=True, null=True)
    company_from = models.CharField(verbose_name='自社出勤時間', max_length=5, blank=True, null=True)
    company_to = models.CharField(verbose_name='自社退勤時間', max_length=5, blank=True, null=True)
    company_rest = models.CharField(verbose_name='自社休憩時間', max_length=5, blank=True, null=True)
    company_work = models.CharField(verbose_name='自社稼働時間', max_length=5, blank=True, null=True)
    office_flg = models.CharField(verbose_name='勤務フラグ', max_length=1, choices=OFFICE_FLG_CHOICES)
    office_other_time = models.CharField(verbose_name='その他時間', max_length=3, blank=True, null=True)
    note = models.CharField(verbose_name='備考', max_length=100, blank=True, null=True)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    created_id = models.IntegerField(verbose_name='作成者id', validators=[MaxLengthValidator(11)])
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    updated_id = models.IntegerField(verbose_name='更新者id', validators=[MaxLengthValidator(11)])
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_office_time'
        verbose_name = verbose_name_plural = "勤務時間"
        unique_together = (('user', 'office_yy_mm', 'office_date'),)

    def __str__(self):
        return self.office_yy_mm


class TOfficeTimeHistory(models.Model):
    HOLIDAY_CHOICES = (
        ('0', '平日'),
        ('1', '土曜'),
        ('2', '日曜、祝日、祝祭日'),
    )
    OFFICE_FLG_CHOICES = (
        ('0', '通常勤務'),
        ('1', '休日'),
        ('2', '有給'),
        ('3', '欠勤'),
        ('4', '特別休日'),
        ('5', '振休'),
    )
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    office_yy_mm = models.CharField(verbose_name='勤務年月', max_length=6)
    office_date = models.IntegerField(verbose_name='日付', validators=[MaxLengthValidator(2)])
    office_day = models.CharField(verbose_name='曜日', max_length=1)
    history_id = models.IntegerField(verbose_name='履歴ID', validators=[MaxLengthValidator(11)], help_text="勤務時間テーブルのユニーク毎にインクリメント")
    holiday_flg = models.CharField(verbose_name='祝日フラグ', max_length=1, choices=HOLIDAY_CHOICES)
    office_from = models.CharField(verbose_name='常駐先出勤時間', max_length=5, blank=True, null=True)
    office_to = models.CharField(verbose_name='常駐先退勤時間', max_length=5, blank=True, null=True)
    office_rest = models.CharField(verbose_name='常駐先休憩時間', max_length=5, blank=True, null=True)
    office_work = models.CharField(verbose_name='常駐先稼働時間', max_length=5, blank=True, null=True)
    company_from = models.CharField(verbose_name='自社出勤時間', max_length=5, blank=True, null=True)
    company_to = models.CharField(verbose_name='自社退勤時間', max_length=5, blank=True, null=True)
    company_rest = models.CharField(verbose_name='自社休憩時間', max_length=5, blank=True, null=True)
    company_work = models.CharField(verbose_name='自社稼働時間', max_length=5, blank=True, null=True)
    office_flg = models.CharField(verbose_name='勤務フラグ', max_length=1, choices=OFFICE_FLG_CHOICES)
    office_other_time = models.CharField(verbose_name='その他時間', max_length=3, blank=True, null=True)
    note = models.CharField(verbose_name='備考', max_length=100, blank=True, null=True)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    created_id = models.IntegerField(verbose_name='作成者id', validators=[MaxLengthValidator(11)])
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    updated_id = models.IntegerField(verbose_name='更新者id', validators=[MaxLengthValidator(11)])
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_office_time_history'
        verbose_name = verbose_name_plural = "勤務時間履歴"
        unique_together = (('user', 'office_yy_mm', 'office_date', 'history_id'),)

    def __str__(self):
        return self.office_yy_mm



class TApprovalStatus(models.Model):
    BUSINESS_FLG_CHOICES = (
        ('0', '勤怠'),
        ('1', '有給'),
    )
    APPROVAL_STATUS_CHOICES = (
        ('0', '申請中'),
        ('1', '承認済'),
        ('2', '差戻し'),
    )
    approval_status_id = models.AutoField(verbose_name='承認状況id', primary_key=True, help_text="自動採番")
    approver_id = models.IntegerField(verbose_name='承認者id', blank=True, null=True, validators=[MaxLengthValidator(11)])
    applicant_id = models.IntegerField(verbose_name='申請者id', validators=[MaxLengthValidator(11)])
    business_flg = models.CharField(verbose_name='業務フラグ', max_length=1, choices=BUSINESS_FLG_CHOICES)
    approval_status = models.CharField(verbose_name='承認状況', max_length=1, choices=APPROVAL_STATUS_CHOICES)
    approval_comment = models.CharField(verbose_name='承認コメント', max_length=100, blank=True, null=True,)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    created_id = models.IntegerField(verbose_name='作成者id', validators=[MaxLengthValidator(11)])
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    updated_id = models.IntegerField(verbose_name='更新者id', validators=[MaxLengthValidator(11)])
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_approval_status'
        verbose_name = verbose_name_plural = "承認状況"
        indexes = [
            models.Index(fields=['applicant_id', 'business_flg'], name='index_t_approval_status'),
        ]

    def __str__(self):
        return str(self.approval_status_id)


class MEizeHoliday(models.Model):
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    eize_holiday_date = models.DateField(verbose_name='イーゼ祝祭日')
    eize_holiday_name = models.CharField(verbose_name='イーゼ祝祭日名', max_length=256, unique_for_date=eize_holiday_date)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 'm_eize_holiday'
        verbose_name = verbose_name_plural = "祝祭日マスタ"
        unique_together = (('user', 'eize_holiday_date'),)

    def __str__(self):
        return self.eize_holiday_name


class TSalariedStock(models.Model):
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    salaried_stock = models.DecimalField(verbose_name='有給残日数', max_digits=3, decimal_places=1)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_salaried_stock'
        verbose_name = verbose_name_plural = "有給残日数"
        unique_together = (('user'),)

    def __str__(self):
        return str(self.user)

class TSalaried(models.Model):
    SALARIED_FLG_CHOICES = (
        ('0', '有給'),
        ('1', '代休・振替休日'),
        ('2', '欠勤'),
        ('3', '誕生日・結婚記念日'),
        ('4', '慶弔関連'),
        ('5', 'その他'),
    )
    SALARIED_NUM_CHOICES = (
        ('0.5', '半休'),
        ('1.0', '全休'),
    )
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    application_date = models.DateField(verbose_name='有給申請年月日')
    salaried_date = models.DateField(verbose_name='有給取得年月日')
    approval_status_id = models.ForeignKey('TApprovalStatus', verbose_name='承認状況id', to_field="approval_status_id", on_delete=models.CASCADE)
    salaried_flg = models.CharField(verbose_name='有給区分', max_length=1, choices=SALARIED_FLG_CHOICES)
    salaried_num = models.CharField(verbose_name='有給取得数', max_length=3, choices=SALARIED_NUM_CHOICES)
    salaried_from = models.CharField(verbose_name='有給取得時間FROM', max_length=4)
    salaried_to = models.CharField(verbose_name='有給取得時間TO', max_length=4)
    reason = models.CharField(verbose_name='事柄', max_length=500)
    contact_name = models.CharField(verbose_name='連絡先名称', max_length=100, blank=True, null=True)
    contact_tel = models.IntegerField(verbose_name='連絡先TEL', validators=[MaxLengthValidator(11)], blank=True, null=True)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    created_id = models.IntegerField(verbose_name='作成者id', validators=[MaxLengthValidator(11)])
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    updated_id = models.IntegerField(verbose_name='更新者id', validators=[MaxLengthValidator(11)])
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_salaried'
        verbose_name = verbose_name_plural = "有給"
        unique_together = (('user', 'salaried_date'),)

    def __str__(self):
        return str(self.user) + ":" + str(self.salaried_date)


class TSalariedHistory(models.Model):
    SALARIED_FLG_CHOICES = (
        ('0', '有給'),
        ('1', '代休・振替休日'),
        ('2', '欠勤'),
        ('3', '誕生日・結婚記念日'),
        ('4', '慶弔関連'),
        ('5', 'その他'),
    )
    SALARIED_NUM_CHOICES = (
        ('0.5', '半休'),
        ('1.0', '全休'),
    )
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.CASCADE)
    application_date = models.DateField(verbose_name='有給申請年月日')
    salaried_date = models.DateField(verbose_name='有給取得年月日')
    history_id = models.IntegerField(verbose_name='履歴ID', validators=[MaxLengthValidator(11)], help_text="有給テーブルのユニーク毎にインクリメント")
    approval_status_id = models.ForeignKey('TApprovalStatus', verbose_name='承認状況id', to_field="approval_status_id", on_delete=models.CASCADE)
    salaried_flg = models.CharField(verbose_name='有給区分', max_length=1, choices=SALARIED_FLG_CHOICES)
    salaried_num = models.CharField(verbose_name='有給取得数', max_length=3, choices=SALARIED_NUM_CHOICES)
    salaried_from = models.CharField(verbose_name='有給取得時間FROM', max_length=4)
    salaried_to = models.CharField(verbose_name='有給取得時間TO', max_length=4)
    reason = models.CharField(verbose_name='事柄', max_length=500)
    contact_name = models.CharField(verbose_name='連絡先名称', max_length=100, blank=True, null=True)
    contact_tel = models.IntegerField(verbose_name='連絡先TEL', validators=[MaxLengthValidator(11)], blank=True, null=True)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    created_id = models.IntegerField(verbose_name='作成者id', validators=[MaxLengthValidator(11)])
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    updated_id = models.IntegerField(verbose_name='更新者id', validators=[MaxLengthValidator(11)])
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_salaried_history'
        verbose_name = verbose_name_plural = "有給履歴"
        unique_together = (('user', 'salaried_date', 'history_id'),)

    def __str__(self):
        return str(self.user) + ":" + str(self.salaried_date)



class TNotice(models.Model):
    user = models.ForeignKey('User', verbose_name='ユーザー', to_field='user_id', on_delete=models.DO_NOTHING)
    title = models.CharField(verbose_name='タイトル', max_length=255)
    datetime = models.DateTimeField(verbose_name='日時')
    body = models.CharField(verbose_name='内容', max_length=500)
    created = models.DateTimeField(verbose_name='作成日', auto_now_add=True)
    updated = models.DateTimeField(verbose_name='更新日', auto_now=True)
    is_active = models.BooleanField(_('active'), default=True,)

    class Meta:
        db_table = 't_notice'
        verbose_name = verbose_name_plural = "お知らせ"

    def __str__(self):
        return self.title
