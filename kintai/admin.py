from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, MClient, TSalariedStock, MEizeHoliday, TNotice
from django.utils.translation import gettext, gettext_lazy as _
from django import forms

# Register your models here.



class UserAdmin(UserAdmin):
    """ユーザー"""

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {
            'fields': ('last_name', 'first_name', 'email', 'enter_date', 'google_client_id', 'google_client_sercret'),
        }),
        (_('Permissions'), {
            'fields': ('is_superuser', 'is_active', 'groups', 'user_permissions'),
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', ),
        }),
        (_('Personal info'), {
            'fields': ('last_name', 'first_name', 'email', 'enter_date', 'google_client_id', 'google_client_sercret'),
        }),
        (_('Permissions'), {
            'fields': ('is_superuser', 'is_active', 'groups', 'user_permissions'),
        }),
    )
    list_display = ('user_id', 'username', 'email', 'last_name', 'first_name', 'is_superuser', 'is_active')
    list_display_links = ('user_id', 'username', 'email')
    list_filter = ('is_superuser', 'is_active', 'groups')
    list_per_page = 50
    search_fields = ('username', 'first_name', 'last_name', 'email')
    ordering = ('user_id', )
    filter_horizontal = ('groups', 'user_permissions',)


class TSalariedStockAdmin(admin.ModelAdmin):
    """有給残日数"""

    fieldsets = (
        (None, {'fields': ('user', 'salaried_stock', 'created', 'updated',)}),
    )
    add_fieldsets = (
        (None, {'fields': ('user', 'salaried_stock', 'created', 'updated',)}),
    )

    readonly_fields = ('created', 'updated')
    list_display = ('user', 'salaried_stock', 'created', 'updated')
    list_display_links = ('user', 'salaried_stock')
    list_per_page = 50
    ordering = ('user',)
    search_fields = ('user',)


class MClientForm(forms.ModelForm):
    """常駐先マスタフォーム"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['def_work_from'].widget = forms.TextInput()
        self.fields['def_work_to'].widget = forms.TextInput()
        self.fields['def_rest_from'].widget = forms.TextInput()
        self.fields['def_rest_to'].widget = forms.TextInput()

    def clean(self):
        self._validate_unique = True
        datas = self.cleaned_data

        if datas['def_work_from'] and datas['def_work_to']:
            if datas['def_work_from'] > datas['def_work_to']:
                raise forms.ValidationError("標準勤務終了時間は開始時間より後に設定してください")

        if datas['def_rest_from'] and datas['def_rest_to']:
            if datas['def_rest_from'] > datas['def_rest_to']:
                raise forms.ValidationError("標準休憩終了時間は開始時間より後に設定してください")

        return self.cleaned_data

class MClientAdmin(admin.ModelAdmin):
    """常駐先マスタ"""

    form = MClientForm

    fieldsets = (
        (None, {
            'fields': ('client_name', ('def_work_from', 'def_work_to'),
                       ('def_rest_from', 'def_rest_to'), 'is_active', 'created', 'updated',
                       )}),
    )
    add_fieldsets = (
        (None, {
            'fields': ('client_name', ('def_work_from', 'def_work_to'),
                       ('def_rest_from', 'def_rest_to'), 'is_active',
                       )}),
    )

    readonly_fields = ('client_id', 'created', 'updated')
    list_display = ('client_id', 'client_name', 'created', 'updated', 'is_active')
    list_display_links = ('client_id', 'client_name')
    list_filter = ('is_active',)
    list_per_page = 50
    ordering = ('client_id',)
    search_fields = ('client_id', 'client_name')

    class Media:
        css = {'all': ('css/jquery.timepicker.min.css',)}
        js = ('js/jquery-3.4.1.min.js', 'js/jquery.timepicker.min.js', 'js/admin.js')


class MEizeHolidayAdmin(admin.ModelAdmin):
    """祝日マスタ"""

    fieldsets = (
        (None, {'fields': ('user', 'eize_holiday_name', 'eize_holiday_date', 'created', 'updated',)}),
    )
    add_fieldsets = (
        (None, {'fields': ('user', 'eize_holiday_name', 'eize_holiday_date',)}),
    )

    readonly_fields = ('created', 'updated')
    list_display = ('user', 'eize_holiday_name', 'eize_holiday_date', 'created', 'updated')
    list_display_links = ('user', 'eize_holiday_name', 'eize_holiday_date')
    list_per_page = 50
    search_fields = ('user', 'eize_holiday_name')


class TNoticeForm(forms.ModelForm):
    body = forms.CharField(widget=forms.Textarea(attrs={'cols': 100, 'rows': 5, 'maxlength': 500}))

class TNoticeAdmin(admin.ModelAdmin):
    """お知らせ"""
    form = TNoticeForm

    fieldsets = (
        (None, {'fields': ('user', 'title', 'datetime', 'body', 'created', 'updated',)}),
    )
    add_fieldsets = (
        (None, {'fields': ('user', 'title', 'datetime', 'body', 'created', 'updated',)}),
    )

    readonly_fields = ('created', 'updated')
    list_display = ('id', 'user', 'title', 'datetime', 'created', 'updated')
    list_display_links = ('id', 'user', 'title', 'datetime')
    list_per_page = 50
    search_fields = ('user', 'title')

admin.site.register(User, UserAdmin)
admin.site.register(TSalariedStock, TSalariedStockAdmin)
admin.site.register(MClient, MClientAdmin)
admin.site.register(MEizeHoliday, MEizeHolidayAdmin)
admin.site.register(TNotice, TNoticeAdmin)