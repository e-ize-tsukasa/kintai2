$(function(){
	$("#id_def_work_from, #id_def_work_to, #id_def_rest_from, #id_def_rest_to").attr('data-time-format', 'H:i');
	$("#id_def_work_from, #id_def_work_to, #id_def_rest_from, #id_def_rest_to").timepicker({
	    'minTime': '7:00',
		'maxTime': '23:00',
	    'step': 5,
	});
});