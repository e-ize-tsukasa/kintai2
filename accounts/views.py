from django.shortcuts import render

# Create your views here.
from django.conf import settings
from .forms import LoginForm, RegisterForm
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth import views
from axes.utils import reset
from django.shortcuts import resolve_url
from axes.models import AccessAttempt

from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView

import logging
logging.getLogger(__name__)

class LoginView(views.LoginView):
    form_class = LoginForm
    template_name = 'accounts/login.html'

    def get_success_url(self):
        count = reset(username=self.request.user.email)
        return resolve_url(settings.LOGIN_REDIRECT_URL)

def reset(username: str = None) -> int:
    """
    ログイン成功時にアクセスログを削除（5回間違えるまでに成功したらリセット）
    """
    attempts = AccessAttempt.objects.all()
    attempts.filter(username=username)

    count = attempts.delete()
    logging.info('AXES: Reset %s access attempts from database.', count)
    return count


# adminでユーザー登録行うから多分使わない
class SignUpView(generic.CreateView):
    form_class = RegisterForm
    success_url = reverse_lazy('accounts/login')
    template_name = 'accounts/signup.html'


# 以下4つはパスワード忘れた時の処理
class PasswordReset(PasswordResetView):
    """パスワード変更用URLの送付ページ"""
    subject_template_name = 'accounts/mail_template/password_reset/subject.txt'
    email_template_name = 'accounts/mail_template/password_reset/message.txt'
    template_name = 'accounts/password_reset_form.html'
    success_url = reverse_lazy('accounts:password_reset_done')


class PasswordResetDone(PasswordResetDoneView):
    """パスワード変更用URLを送りましたページ"""
    template_name = 'accounts/password_reset_done.html'


class PasswordResetConfirm(PasswordResetConfirmView):
    """新パスワード入力ページ"""
    success_url = reverse_lazy('accounts:password_reset_complete')
    template_name = 'accounts/password_reset_confirm.html'


class PasswordResetComplete(PasswordResetCompleteView):
    """新パスワード設定しましたページ"""
    template_name = 'accounts/password_reset_complete.html'

